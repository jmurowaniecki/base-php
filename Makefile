SERVICE = bash docker/λ

.DEFAULT: help

help:
	@echo "Plano.cc PHP Boilerplate\n\
	\n\
	Opções:\n\
		make install		Instalar a aplicação.\n\
		\n\
		make start		Inicializa os Docker containers.\n\
		make stop		Finaliza os Docker containers.\n\
		\n\
		make fix		Ajusta permissões em diretórios.\n\
		\n\
		make requirements	Remove as pastas de requisitos\n\
					tanto Composer quanto NPM e os\n\
					executa novamente logo a seguir."

start:
	$(SERVICE) up -d

debug:
	$(SERVICE) up

stop:
	$(SERVICE) down

install:
	$(SERVICE) install
