

# Plano Transmedia - PHP Boilerplate

![Codename: JUMPSTART](https://img.shields.io/badge/codename-jumpstart-yellow.svg)
![Version: 0.0.1a](https://img.shields.io/badge/version-0.0.1a-lightgrey.svg)

Utilize como padrão para iniciar seus projetos de forma rápida e descomplicada com **Composer** e **Docker**.

## Guia de Instalação

Execute o comando `composer -sdev create-project planocc/base-php nome_do_seuprojeto` para criar um projeto base na pasta *nome_do_seuprojeto*.

> Lembrando que a opção `-sdev` deve-se apenas para permitir a criação do projeto com a versão mais recente em desenvolvimento.

Após adquirir o projeto, execute o comando `make install` dentro da pasta do projeto para executar as rotinas de instalação e configuração que resultará em uma saída conforme exemplo abaixo:

```
λ@PLANOcc:~$ composer create-project planocc/base-php projeto-teste
λ@PLANOcc:~$ cd projeto-teste
λ@PLANOcc:~/projeto-teste$ make install

docker-compose build
Building php7
Step 1/15 : FROM php:7.1-apache

...
... Relatório extenso do build do Docker Compose.
...
... ATENÇÃO:
...          Esse processo pode demorar um pouco.
...

Removing intermediate container e79886545c15
Successfully built 30bbf113f2d5
Successfully tagged basephp_php7:latest
mariadbdata uses an image, skipping
mariadb uses an image, skipping

λ@PLANOcc:~/projeto-teste$
```

> **Dica:** O comando `make install` executa o build dos containers e, logo a seguir, instala as dependências via `composer` e `npm`. Para mais informações sobre as opções de montagem e automação execute o comando `make` sem parâmetros.


## Primeiros passos

Utilize o comando `make start` para inicializar o projeto e `make stop` para finalizar os containers. A execução do comando `make start` exibirá uma saída similar ao exemplo abaixo:

```
λ@PLANOcc:~/projeto-teste$ make start

docker-compose up -d
Creating network "basephp_pipeline" with the default driver
Creating basephp_mariadbdata_1
Creating basephp_php7_1
Creating basephp_mariadb_1

λ@PLANOcc:~/projeto-teste$
```
**Pronto!** Seu projeto já está executando e pode ser acessado pelo endereço http://localhost/

> **Dica:** se você tiver outros containers utilizando as portas padrão do Apache e MariaDB/MySQL os serviços não serão inicializados. Para corrigir isso será necessário alterar essas portas para outras que estejam livres.
