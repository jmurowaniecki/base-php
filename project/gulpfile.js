'use strict';

const gulp = require('gulp'),
    htmlMinifier = require('gulp-html-minifier'),
    imagemin = require('gulp-imagemin'),
    nunjucksRender = require('gulp-nunjucks-render'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    svgSprite = require('gulp-svg-symbols'),
    ts = require('gulp-typescript'),
    uglify = require('gulp-uglify');

const imageminOptions = [
    imagemin.gifsicle({ interlaced: true }),
    imagemin.jpegtran({ progressive: true }),
    imagemin.optipng({ optimizationLevel: 5 })
];

const sassOptions = {
    'errLogToConsole': true,
    'outputStyle': 'compressed'
};

const svgSpriteOptions = {
    'templates': ['default-svg']
};

const tsOptions = {
    //'allowJs': false,
    'baseUrl': './public/assets',
    'module': 'amd',
    'noFallthroughCasesInSwitch': true,
    'noImplicitAny': true,
    'noImplicitReturns': true,
    'noImplicitThis': true,
    'paths': {
        'jquery': ['plugin/jquery']
    },
    'preserveConstEnums': true,
    'removeComments': true,
    'target': 'ES5'
};

// CSS APP
gulp.task('css-app', () => {
    return gulp.src('./resources/assets/scss/app.scss')
        .pipe(sass.sync(sassOptions).on('error', sass.logError))
        .pipe(gulp.dest('./public/assets/css'));
});

// CSS MODULE
gulp.task('css-module', () => {
    return gulp.src('./resources/assets/scss/module/**/*.scss')
        .pipe(sass.sync(sassOptions).on('error', sass.logError))
        .pipe(gulp.dest('./public/assets/css'));
});

// CSS PLUGIN
gulp.task('css-plugin', () => {
    return gulp.src('./resources/assets/scss/plugin/*.scss')
        .pipe(sass.sync(sassOptions).on('error', sass.logError))
        .pipe(gulp.dest('./public/assets/css/plugin'));
});

// IMAGE
gulp.task('imagemin', () => {
    return gulp.src(['./resources/assets/img/**/*.gif',
            './resources/assets/img/**/*.jpg',
            './resources/assets/img/**/*.jpeg',
            './resources/assets/img/**/*.png'
        ])
        .pipe(imagemin(imageminOptions))
        .pipe(gulp.dest('./public/assets/img'))
});

// JS COMMON
gulp.task('js-common', () => {
    return gulp.src('./resources/assets/js/common/*.js')
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .on('error', (err) => { console.log(err.message); })
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./public/assets/js/common'));
});

// JS MODULE
gulp.task('js-module', () => {
    return gulp.src('./resources/assets/js/module/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .on('error', (err) => { console.log(err.message); })
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./public/assets/js/module'));
});

// SVG SPRITE
gulp.task('svg-sprite', () => {
    return gulp.src('./resources/assets/img/svg/*.svg')
        .pipe(svgSprite(svgSpriteOptions))
        .on('error', (err) => { console.log(err.message); })
        .pipe(gulp.dest('./public/assets/img'));
});

// TS COMMON
gulp.task('ts-common', () => {
    return gulp.src('./resources/assets/ts/common/*.ts')
        .pipe(ts(tsOptions))
        .pipe(gulp.dest('./resources/assets/js/common'));
});

// TS MODULE
gulp.task('ts-module', () => {
    return gulp.src('./resources/assets/ts/module/**/*.ts')
        .pipe(ts(tsOptions))
        .pipe(gulp.dest('./resources/assets/js/module'));
});

// WATCH
gulp.task('watch', () => {
    //gulp.watch(['./resources/img/**/*.gif',
        //'./resources/assets/img/**/*.jpg',
        //'./resources/assets/img/**/*.jpeg',
        //'./resources/assets/img/**/*.png',
        //'./resources/assets/img/svg/*.svg'], ['imagemin']);
    //gulp.watch('./resources/assets/img/svg/*.svg', ['svg-sprite']);
    gulp.watch(['./resources/assets/scss/app.scss',
        './resources/assets/scss/common/*.scss',
        './resources/assets/scss/layout/*.scss',
        './resources/assets/scss/module/*.scss',
        './resources/assets/scss/flexboxgrid/*.scss'
    ], ['css-app']);
    gulp.watch('./resources/assets/scss/module/**/*.scss', ['css-module']);
    gulp.watch('./resources/assets/scss/plugin/*.scss', ['css-plugin']);
    gulp.watch('./resources/assets/ts/common/*.ts', ['ts-common']);
    gulp.watch('./resources/assets/ts/module/**/*.ts', ['ts-module']);
    gulp.watch('./resources/assets/js/common/*.js', ['js-common']);
    gulp.watch('./resources/assets/js/module/**/*.js', ['js-module']);
});

gulp.task('default', ['css-app', 'css-module', 'ts-common', 'ts-module', 'js-common', 'js-module', 'watch']);
